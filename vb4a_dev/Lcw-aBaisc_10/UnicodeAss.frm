VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form UnicodeAss 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4680
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Command1 
      Caption         =   "生成"
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   2520
      Width           =   4455
   End
   Begin MSForms.TextBox TextBox1 
      Height          =   2295
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      VariousPropertyBits=   746604571
      Size            =   "7858;4048"
      FontName        =   "宋体"
      FontHeight      =   180
      FontCharSet     =   134
      FontPitchAndFamily=   34
   End
End
Attribute VB_Name = "UnicodeAss"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    Clipboard.Clear
    Clipboard.SetText (genUni)
    TextBox1.text = genUni
    MsgBox Form1.CHorEN(Form1.CH, "已复制到剪贴板!|Copied!"), vbInformation, ""
End Sub

Public Function genUni() As String

    Dim i As Long
    Dim chrTmp$
    Dim ByteLower$, ByteUpper$
    Dim strReturn$ '存储转换后的编码
    strEncode = TextBox1.text
    For i = 1 To Len(strEncode)
        chrTmp$ = Mid(strEncode, i, 1)
        ByteLower$ = Hex$(AscB(MidB$(chrTmp$, 1, 1)))
        If Len(ByteLower$) = 1 Then ByteLower$ = "0" & ByteLower$
        ByteUpper$ = Hex$(AscB(MidB$(chrTmp$, 2, 1)))
        If Len(ByteUpper$) = 1 Then ByteUpper$ = "0" & ByteUpper$
        strReturn$ = strReturn$ & "\u" & ByteUpper$ & ByteLower$
    Next
    
    genUni = strReturn$

End Function
